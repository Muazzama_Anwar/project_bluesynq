from pymongo import MongoClient
import pymongo
import dateutil.parser

class database():
	connection = MongoClient(connect = False)
	db = connection['bluesynq_project']
	topics = db['topics']
	tweets = db['tweets']

#insert topics
	def insertOne(self, topics, date):
	    query = {"topics":topics, "date": date }
	    try:
	      self.topics.insert_one(query)
	      print("Topics Inserted")
	    except pymongo.errors.DuplicateKeyError:
	      pass

#insert tweets data
	def insertTweets(self, topic, tweet_id, text, created_at, fav, retweet, \
    in_reply_id, in_reply_name,source, user_id, user,user_location, \
    followers, following, tweets_count, polarity, screen_name, description,\
	verified, listed_count, user_created_at, time_zone):

	    query = {"_id" : tweet_id,
	    "topic": topic,
	    "tweet_text" : text,
		"created_at" :  dateutil.parser.parse(created_at, ),
		"favourites_count" : fav,
		"retweet_count" : retweet,
		"in_reply_id": in_reply_id,
		"in_reply_name": in_reply_name,
		"source": source,
		"polarity" : polarity,
		"User" : {
	                                      "user_id" : user_id,          
	                                      "user" : user,        
	                                      "location" : user_location,          
	                                      "followers_count" : followers,
	                                      "following": following, 
	                                      "tweets_count":tweets_count,
	                                      "screen_name": screen_name,
										    "description": description,
										    "verified": verified,										    
										    "listed_count": listed_count,
										    "user_created_at": dateutil.parser.parse(user_created_at, ),
										    "time_zone": time_zone
	                                      }
	            	}
	    try:
	      self.tweets.insert_one(query)
	    except pymongo.errors.DuplicateKeyError:
	      pass

#retrieve locations of users for a specific topic
	def location_list(self, topic):
		query = {"topic":topic}
		project = {"User.location":1, "_id":0}
		try:
			location_dic = self.tweets.find(query, project)
			return location_dic
		except Exception as e:
			print(e)	


#retrieve tweets for a specific topic
	def retrieve_tweets(self, topic):
		query = {"topic": topic}
		try:
			tweets = self.tweets.find(query).limit(10)
			return tweets
		except Exception as e:
			print(e)


#retrieve how many distinct users are posting about specific topic
	def distinct_users(self, topic):
		query = {"topic":topic}
		try:
			users = self.tweets.distinct("User.screen_name", query)
			return users
		except Exception as e:
			print(e)

#tweets per user for specific topic
	def retrieve_user_tweets(self, topic, user):
		query = {"topic": topic, "User.screen_name":user}
		project  = {"_id":1, "tweet_text":1, "polarity":1, "User":1}
		try:
			details = self.tweets.find(query, project)
			return details
		except Exception as e:
			print(e)
			
			

	    	  