from app.database import database
import collections
from textblob import TextBlob
import re
import tweepy

db = database()

class processing():
	
	auth =tweepy.OAuthHandler('dYcgcIAJ8sArCujKzWLuUYCvQ', 'Suvhk2X0q8kPk9gEQychwQnNW2amWjPnYcCuhbQlEI0pXhIqGu')
	auth.set_access_token('3070252507-vbExZWPWA4LtcFrjesJUSKHOlBBceZNvUjJM5KJ', 'VpOO8VkBdUJlmecLPgQ0NNo8OIOJwlhm8956E1J66y9q2')
	twitter = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)

	def get_trends(self):
		results = self.twitter.trends_place(23424922)
		trend_list = []
		for location in results:
			for trend in location["trends"]:
				trend_list.append(trend["name"])
		return trend_list
	    

	def primary_chatters(self, topic):
		user_details = self.user_details(topic)
		pc_list = []
		user_list = []
		for u in user_details:
			if(u['count']>2):
				pc_dict = {}
				pc_dict['screen_name'] = u['screen_name']
				pc_dict['user_id'] = u['user_id']
				pc_list.append(pc_dict)		
		print(pc_list) #primary chatters	
		return pc_list

	def followers(self, topic):
		user_list = []
		pc_list = self.primary_chatters(topic)
		#collectiong followers
		for u in pc_list:
			user_dict = {}
			followers_list = []
			tweet_id_list = []
			tweets = db.retrieve_user_tweets(topic, u['screen_name'])
			for t in tweets:
				tweet_id_list.append(t['_id'])
			# cursor = tweepy.Cursor(self.twitter.followers_ids, screen_name=u['screen_name']).items()
			# for follower in cursor:
			# 	followers_list.append(follower)	
			user_dict['user_id'] = u['user_id']	
			user_dict['screen_name'] = u['screen_name']
			user_dict['tweets'] = tweet_id_list
			user_dict['followers'] = followers_list
			user_list.append(user_dict)
		#print(user_list)
		return user_list
		#user_list = list of user_dict, 
		#user_dict contains username, list of tweet IDs, list of followers for a particulat user  	

	def get_mentions(self, topic):
		user_list = self.followers(topic)	
		for u in user_list:
			print(u['tweets'])
			for t in u['tweets']:
				print("tweet_id: "+t)
				cursor = tweepy.Cursor(self.twitter.search, q="@"+u['screen_name']).items()
				for mention in cursor:
					if(mention.text.startswith("RT")==False):
						if(mention.in_reply_to_status_id_str == t):
							print("matched tweet: "+ mention.text)
						else:
							None


	def replies_of_followers(self, user_list):
		try:
			for user in user_list:
				screen_name = user['screen_name']
				user_id = user['user_id']
				print("screen name: "+screen_name)
				print("user id: "+ user_id)
				t_list = user['tweets']
				f_list = user['followers']
				print(t_list)
				for t in t_list:
					print("tweet id: "+t)
					for f in f_list:
						print("follower id: "+ str(f))
						for page in tweepy.Cursor(self.twitter.user_timeline, id=str(f)).pages(1):
							for item in page:
								print("in reply status id: "+str(item.in_reply_to_status_id_str))
								print("in reply user id: "+str(item.in_reply_to_user_id_str))
								print("in reply screen name: "+str(item.in_reply_to_screen_name))
								if item.in_reply_to_status_id_str == t or item.in_reply_to_user_id_str == str(user_id) or item.in_reply_to_screen_name == str(screen_name):
									print("matched")
									last_tweet = item
									while(True):
										print("tweet: "+last_tweet.text)
										prev_tweet = self.twitter.get_status(last_tweet.in_reply_to_status_id_str)
										last_tweet = prev_tweet
										if not last_tweet.in_reply_to_status_id:
											break
										pass        
		except tweepy.TweepError as e:
			pass        
	            
	#location of chatters	
	def location_list(self, topic):
		location_dic = db.location_list(topic)
		loc_list = []
		for user in location_dic:
			loc_list.append(user['User']['location'])
		loc_dic = collections.Counter(loc_list).most_common(15)
		#print(loc_dic)
		return loc_dic

	# def cleaning(self, tweet):
	# 	tweet = p.clean(tweet).encode('ascii', 'ignore').decode('ascii')
	# 	tweet = tweet.replace('"', '')
	# 	return tweet
	def processTweet(self, tweet):
    # process the tweets

	    #Convert to lower case
	    tweet = tweet.lower()
	    #Convert www.* or https?://* to URL
	    tweet = re.sub(r'^https?:\/\/.*[\r\n]*', '', tweet, flags=re.MULTILINE)

	    #Convert @username to AT_USER
	    tweet = re.sub('@[^\s]+',' ',tweet)
	    #Remove additional white spaces
	    tweet = re.sub('[\s]+', ' ', tweet)
	    #Replace #word with word
	    tweet = re.sub(r'#([^\s]+)', r'\1', tweet)
	    #trim
	    tweet = tweet.strip('\'"')
	    return tweet

	def polarity(self, text):
		sent = TextBlob(text)
		polarity = sent.sentiment.polarity
		return polarity    

#retrieve word frequency for a specific topic
	def word_freq(self, topic):
		s = ""   
		tweets = db.retrieve_tweets(topic)
		main_word_list = []
		for t in tweets:
			tweet = t['tweet_text']
			tweet = self.processTweet(tweet)
			s+=str(tweet)
			sent = TextBlob(tweet)
			word_list = sent.words
			for words in word_list:
				main_word_list.append(words)
		word_count = collections.Counter(main_word_list).most_common(25)
		#print(word_count)
		word_list1 = []
		for t in word_count:
			d = {}
			d['text'] = t[0]
			d['size'] = t[1]
			word_list1.append(d)
		return s, word_list1
			
#retrieve tweets for particular topic			
	def tweets(self, topic):
		texts = db.retrieve_tweets(topic)
		list_of_positive = []
		pos = neg = neu =0
		for t in texts:
			d = {}
			user_id = t['User']['user_id']
			username = t['User']['screen_name']
			polarity =  t['polarity']
			tweet = t['tweet_text']
			date = t['created_at']
			tweet_id = t['_id']
			reply_id = t['in_reply_id']
			d['username'] = username
			d['Polarity'] = polarity
			d['Tweet'] = tweet
			d['Date'] = date
			d['tweet_id'] = tweet_id
			d['in_reply_id'] = reply_id
			d['user_id'] = user_id
			list_of_positive.append(d)
			if polarity > 0:      
				pos = pos+1
			elif polarity < 0:
				neg = neg+1   
			else:
				neu = neu+1
		tweet_count = pos+neg+neu
		return list_of_positive, tweet_count            

#to find primary chatters
	def user_details(self, topic):
		users = db.distinct_users(topic)
		user_list = []
		for user in users:
			details = db.retrieve_user_tweets(topic, user)
			count = 0 #tweet count per user
			user_dict = {}
			for d in details:
				tweet = d['tweet_text']
				polarity = d['polarity']
				user_id = d['User']['user_id']
				user_name = d['User']['screen_name']
				followers = d['User']['followers_count']
				tweet_count = d['User']['tweets_count']
				location = d['User']['location']
				count +=1
			user_dict['user_id'] = user_id	
			user_dict['screen_name'] = user_name
			user_dict['count'] = count
			user_list.append(user_dict)
		return user_list		

	   